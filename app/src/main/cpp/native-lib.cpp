#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_smalltests_addnativecppcode_NativeComm_getTestKey(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_smalltests_addnativecppcode_NativeComm_getAppPassword(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Another hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_smalltests_addnativecppcode_Keys_getGoogleApiKey(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "One More Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_smalltests_addnativecppcode_Keys_getSampleInt(
        JNIEnv* env,
        jobject obj,
        jint number) {
    return number * 4 ;
}

extern "C" JNIEXPORT double JNICALL
Java_com_example_smalltests_addnativecppcode_Keys_getSecretNumber(
        JNIEnv* env,
        jobject obj,
        jdouble number) {
    return 9723.233;
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_smalltests_addnativecppcode_Keys_getTempString(
        JNIEnv* env,
        jobject obj,
        jint number) {
    return reinterpret_cast<jstring>(238761);
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_smalltests_addnativecppcode_CppMainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "One Extra Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
