package com.example.smalltests

import android.app.Application
import android.content.Context
import com.example.smalltests.koin_practice.dbModule
import com.example.smalltests.koin_practice.viewModelModule
import com.example.smalltests.multilingual.LocalHelper
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppClass: Application() {


    companion object{
        lateinit var ctx : Context
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocalHelper.onAttach(base,"en"))
    }

    override fun onCreate() {
        super.onCreate()
        ctx = this
        /*startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@AppClass)
            modules(listOf(dbModule, viewModelModule))
        }*/
    }
}