package com.example.smalltests

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.smalltests.addnativecppcode.CppMainActivity
import com.example.smalltests.android_q_testing.AndroidQTesting
import com.example.smalltests.annotation_test.AnnotationTestMainActivity
import com.example.smalltests.binding_adapters_test.BindingAdaptersActivity
import com.example.smalltests.biometric_test.BiometricMainActivity
import com.example.smalltests.coroutines_test.CoroutinesTestMainActivity
import com.example.smalltests.encrypt_decrypt.EncryptDecryptActivity
import com.example.smalltests.enum_vs_sealed_test.EnumVsSealedTestActivity
import com.example.smalltests.ext_functions.ExtensionFunctionsActivity
import com.example.smalltests.java_value_ref_type.JavaTypeMainActivity
import com.example.smalltests.koin_practice.KoinActivity
import com.example.smalltests.kotlin_tests.KotlinTestsMainActivity
import com.example.smalltests.material_tab.MaterialTabsActivity
import com.example.smalltests.multilingual.MultilingualActivity
import com.example.smalltests.mvvm.MVVMActivity
import com.example.smalltests.progress_bars.ProgressBarActivity
import com.example.smalltests.ssl_pinning_test.SSLPinningTestActivity
import com.example.smalltests.strategy_pattern.CompanyActivity
import com.example.smalltests.theme_test.ThemeTestMainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.single_item_main_screen_buttons.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lv.divider = null
        lv.adapter = Listing(
            arrayListOf(
                ClassModel("Theme", ThemeTestMainActivity::class.java),
                ClassModel("Enum vs Sealed", EnumVsSealedTestActivity::class.java),
                ClassModel("Coroutines Test", CoroutinesTestMainActivity::class.java),
                ClassModel("SSL Pinning Test", SSLPinningTestActivity::class.java),
                ClassModel("Extension Functions", ExtensionFunctionsActivity::class.java),
                ClassModel("Koin Dependency Test", KoinActivity::class.java),
                ClassModel("Annotation Creation Test", AnnotationTestMainActivity::class.java),
                ClassModel("Java Value/Reference Type", JavaTypeMainActivity::class.java),
                ClassModel("Encrypt/Decrypt", EncryptDecryptActivity::class.java),
                ClassModel("Kotlin programs", KotlinTestsMainActivity::class.java),
                ClassModel("Adding C++ Native code to project", CppMainActivity::class.java),
                ClassModel("Biometric Test", BiometricMainActivity::class.java),
                ClassModel("MVVM Binding Adapters", BindingAdaptersActivity::class.java),
                ClassModel("MVVM Update/Notify View When Object with LiveData", MVVMActivity::class.java),
                ClassModel("Strategy Pattern", CompanyActivity::class.java),
                ClassModel("Android Q Testing", AndroidQTesting::class.java),
                ClassModel("MultiLingual", MultilingualActivity::class.java),
                ClassModel("Material Tabs", MaterialTabsActivity::class.java),
                ClassModel("Progress Bars", ProgressBarActivity::class.java)
            )
        )
    }

    inner class Listing(private val list: ArrayList<ClassModel>) : BaseAdapter() {
        override fun getItem(p0: Int) = list[p0]

        override fun getItemId(p0: Int) = p0.toLong()

        override fun getCount() = list.size

        override fun getView(p0: Int, p1: View?, p2: ViewGroup): View {
            val v = LayoutInflater.from(p2.context).inflate(R.layout.single_item_main_screen_buttons, p2, false)

            v.btns.text = list[p0].btnTitle

            v.btns.setOnClickListener {
                startActivity(Intent(this@MainActivity, list[p0].clazz))
            }
            return v
        }
    }

    data class ClassModel(val btnTitle: String, val clazz: Class<*>)
}
