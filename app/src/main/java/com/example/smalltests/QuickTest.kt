package com.example.smalltests

import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main() = runBlocking{
    val time = measureTimeMillis {
        val m1 = launch { msg1() }
        val m2 = launch { msg2() }
        println("${m1.join()}, ${m2.join()}")
    }
    println("time: $time")
}

suspend fun msg1(): String {
    delay(1000)
//    print("msg 1 executed")
    return "message 1..."
}

suspend fun msg2(): String {
    delay(1000)
//    print("msg 2 executed")
    return "message 2..."
}

private fun threadName() = Thread.currentThread().name