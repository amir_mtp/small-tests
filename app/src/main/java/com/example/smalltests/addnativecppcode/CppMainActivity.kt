package com.example.smalltests.addnativecppcode

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.StringBuilder

class CppMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val sb = StringBuilder()

        val nc = NativeComm()
        sb.append(nc.testKey)
        sb.append("\n")
        sb.append(nc.appPassword)
        sb.append("\n")
        sb.append(Keys().getGoogleApiKey())
        sb.append("\n")
        sb.append(Keys().getSampleInt(34))
        sb.append("\n")
        sb.append(Keys().getSecretNumber())
        sb.append("\n")
        sb.append(Keys().getTempString())
        sb.append("\n")
        sb.append(stringFromJNI())

        statusTV.text = sb.toString()
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}