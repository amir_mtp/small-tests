package com.example.smalltests.addnativecppcode

class Keys {
    external fun getGoogleApiKey(): String
    external fun getSampleInt(num: Int) : Int
    external fun getSecretNumber() : Double
    external fun getTempString() : Int
}