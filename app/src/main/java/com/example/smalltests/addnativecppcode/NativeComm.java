package com.example.smalltests.addnativecppcode;

public class NativeComm {
    public native String getTestKey();
    public native String getAppPassword();
}
