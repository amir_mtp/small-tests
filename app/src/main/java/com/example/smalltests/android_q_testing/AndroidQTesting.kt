package com.example.smalltests.android_q_testing

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.Exception
import java.util.*

class AndroidQTesting: AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val androidID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        val uuID = UUID.randomUUID().toString()

        val tm = (getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager)
        val deviceId = try {
            tm.deviceId
        } catch (e: Exception){
            e.printStackTrace()
            "${e.message}"
        }
        val imei = try {
            tm.imei
        } catch (e :Exception){
            e.printStackTrace()
            "${e.message}"
        }
        /*val meid= try {
            tm.meid
        } catch (e : Exception){
            e.printStackTrace()
            "${e.message}"
        }*/
        val subscriberID= try {
            tm.subscriberId
        } catch (e : Exception){
            e.printStackTrace()
            "${e.message}"
        }



        statusTV.text = "Using Setting.Secure\nAndroid ID: $androidID\n\nUUID: $uuID\n\n\nUsing TelephonyManager:\n" +
                "IMSI for a GSM phone: $subscriberID\n\nDevice ID: $deviceId\n\nIMEI: $imei\n\nMEID: "
    }
}