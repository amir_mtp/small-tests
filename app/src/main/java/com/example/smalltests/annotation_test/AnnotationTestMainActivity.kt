package com.example.smalltests.annotation_test

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.bindview_api.BindView
import com.example.smalltests.R

class AnnotationTestMainActivity : AppCompatActivity() {

    @BindView(R.id.testTV1)
    lateinit var tv: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_annotation_test)

        ViewBinder.bind(this)
        
        tv.text = "Testingg...."

    }
}