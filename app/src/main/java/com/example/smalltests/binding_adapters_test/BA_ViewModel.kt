package com.example.smalltests.binding_adapters_test

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class BA_ViewModel : ViewModel() {

    var name = MutableLiveData<String>().apply { value = "Start Name" }
    var newImage = MutableLiveData<Int>().apply { value = 0 }

    fun changeData(no: Int){
        name.value = "Name Changed"
        newImage.value = no
    }

    fun getRandomNumber() = Random.nextInt(0,4)
}