package com.example.smalltests.binding_adapters_test

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class BA_ViewModel2 : ViewModel() {

    var description = MutableLiveData<String>().apply { value = "No Description!" }
    var newImage = MutableLiveData<Int>().apply { value = 0 }

    fun changeData(no: Int){
        newImage.value = no
        description.value = when(no){
            1 -> "Awesome Desc!"
            2 -> "Wow Description!!"
            else -> "Fine Description"
        }
    }

    fun getRandomNumber() = Random.nextInt(0,4)
}