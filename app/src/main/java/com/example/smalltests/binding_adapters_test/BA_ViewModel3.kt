package com.example.smalltests.binding_adapters_test

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class BA_ViewModel3 : ViewModel() {

    var items = MutableLiveData<List<String>>().apply { value = emptyList() }

    fun changeData(){
        val r = Random.nextInt(0,25)
        val list = ArrayList<String>()
        for (i in 0..r){
            list.add("Ran ${i*2} dom $i")
        }
        items.value = list
    }
}