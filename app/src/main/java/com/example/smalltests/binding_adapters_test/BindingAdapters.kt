package com.example.smalltests.binding_adapters_test

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.example.smalltests.AppClass
import com.example.smalltests.R

object BindingAdapters {

    @BindingAdapter("app:nameField")
    @JvmStatic
    fun setName(textView: TextView, text: String){
        textView.text = text
    }

    @BindingAdapter(value = ["app:desc", "app:newColor"], requireAll = false)
    @JvmStatic
    fun setDescription(textView: TextView, description: String, @ColorRes drawable: Int){
        textView.text = description
//        textView.setTextColor(ContextCompat.getColor(AppClass.ctx, drawable))
    }

    @BindingAdapter("abc")
    @JvmStatic
    fun changeImage(imageView: ImageView, codeToChangeImage: Int){
        when(codeToChangeImage){
            1 -> {
                imageView.setImageResource(R.drawable.carry_van)
            }
            2 -> {
                imageView.setImageResource(R.drawable.courier)
            }
            3 -> {
                imageView.setImageResource(R.drawable.bhejdo_no_caption)
            }
            else -> {
                imageView.setImageResource(R.drawable.bhejdo)
            }
        }
    }

    @BindingAdapter("app:items")
    @JvmStatic
    fun setItemsInRecyclerView(listView: ListView, items: List<String>){
        with(listView.adapter as ListAdapter){
            replaceData(items)
        }
    }
}