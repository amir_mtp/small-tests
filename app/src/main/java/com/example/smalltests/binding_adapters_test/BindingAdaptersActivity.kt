package com.example.smalltests.binding_adapters_test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.smalltests.R
import com.example.smalltests.databinding.ActivityBindingAdaptersTestBinding

class BindingAdaptersActivity : AppCompatActivity() {

    lateinit var binding : ActivityBindingAdaptersTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_binding_adapters_test)
        binding.viewmodel = ViewModelProviders.of(this).get(BA_ViewModel::class.java)
        binding.viewmodel2 = ViewModelProviders.of(this).get(BA_ViewModel2::class.java)
        val vm3 = ViewModelProviders.of(this).get(BA_ViewModel3::class.java)
        binding.viewmodel3 = vm3
        binding.lifecycleOwner = this

        binding.dataListView.adapter = ListAdapter(arrayListOf(),vm3)
    }
}