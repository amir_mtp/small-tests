package com.example.smalltests.binding_adapters_test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.example.smalltests.databinding.SingleItemListAdapterBinding

class ListAdapter(var dataList: List<String>, var viewModel3: BA_ViewModel3): BaseAdapter() {
    override fun getCount() = dataList.size

    override fun getItem(position: Int) = dataList[position]

    override fun getItemId(position: Int) = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val binding : SingleItemListAdapterBinding
        binding =if(convertView == null){
            val inflater = LayoutInflater.from(parent?.context)
            SingleItemListAdapterBinding.inflate(inflater, parent, false)
        } else {
            DataBindingUtil.getBinding(convertView) ?:throw  IllegalStateException()
        }

        with(binding){
            name = dataList[position]
            executePendingBindings()
        }
        return binding.root
    }

    fun replaceData(data: List<String>){
        this.dataList = data
        notifyDataSetChanged()
    }
}