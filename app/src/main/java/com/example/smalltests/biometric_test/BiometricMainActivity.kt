package com.example.smalltests.biometric_test

import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.smalltests.R
import com.example.smalltests.util.Applog
import kotlinx.android.synthetic.main.activity_biometric.*
import java.util.concurrent.Executor
import android.security.keystore.KeyProperties
import android.security.keystore.KeyGenParameterSpec
import java.nio.charset.Charset
import java.security.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


class BiometricMainActivity : AppCompatActivity() {

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_biometric)
        executor = ContextCompat.getMainExecutor(this)

        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    when (errorCode){
                        BiometricPrompt.ERROR_NEGATIVE_BUTTON -> {
                            //loginWithPassword()
                            Applog.toast("Start authentication with login and password")
                        }
                        else -> {
                            //default
                        }
                    }
                    statusTV.text = "Authentication error: $errorCode > $errString"
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)

                    val encryptedInfo: ByteArray? = result.cryptoObject?.cipher?.doFinal(
                        "TEsttttt asdlfh askdjfh alskdjf lasdk jflasdj fl".toByteArray(Charset.defaultCharset()))
                    statusTV.text = "Encrypted information: ${Arrays.toString(encryptedInfo)}"

//                    statusTV.text =
//                        "Authentication success: ${result.cryptoObject?.cipher?.parameters?.provider?.info}"
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    statusTV.text = "Authentication failed"
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
//            .setDeviceCredentialAllowed(true)
//            .setConfirmationRequired(true)
            .setDescription("This is just a short description. you can add your own")
            .build()

        clickMeBtn.setOnClickListener {
//            biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(getEncryptCipher(createKey())))

            //region Using from Google Example
            biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(getCipher().apply {
                init(Cipher.ENCRYPT_MODE, getSecretKey())
            }))
            //endregion
        }

        //Listener for detecting Biometric feature in the device
        when (BiometricManager.from(this).canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> statusTV.text =
                "App can authenticate using biometrics."
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> statusTV.text =
                "No biometric features available on this device."
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> statusTV.text =
                "Biometric features are currently unavailable."
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> statusTV.text =
                "The user hasn't associated any biometric credentials with their account."
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            generateSecretKey(KeyGenParameterSpec.Builder(
                "MY_TEST_KEY",
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .setUserAuthenticationRequired(true)
                // Invalidate the keys if the user has registered a new biometric
                // credential, such as a new fingerprint. Can call this method only
                // on Android 7.0 (API level 24) or higher. The variable
                // "invalidatedByBiometricEnrollment" is true by default.
                .setInvalidatedByBiometricEnrollment(true)
                .build())
        }
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidAlgorithmParameterException::class
    )
    private fun createKey(): SecretKey {
        val algorithm = KeyProperties.KEY_ALGORITHM_AES
        val provider = "AndroidKeyStore"
        val keyGenerator = KeyGenerator.getInstance(algorithm, provider)
        val keyGenParameterSpec = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyGenParameterSpec.Builder(
                "MY_KEY",
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .setUserAuthenticationRequired(true)
                .build()
        } else {
            TODO("VERSION.SDK_INT < M")
        }

        keyGenerator.init(keyGenParameterSpec)
        return keyGenerator.generateKey()
    }

    @Throws(
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class
    )
    private fun getEncryptCipher(key: Key): Cipher {
        val algorithm = KeyProperties.KEY_ALGORITHM_AES
        val blockMode = KeyProperties.BLOCK_MODE_CBC
        val padding = KeyProperties.ENCRYPTION_PADDING_PKCS7
        val cipher = Cipher.getInstance("$algorithm/$blockMode/$padding")
        cipher.init(Cipher.ENCRYPT_MODE, key)
        return cipher
    }

    //region Example from Google https://developer.android.com/training/sign-in/biometric-auth
    private fun generateSecretKey(keyGenParameterSpec: KeyGenParameterSpec) {
        val keyGenerator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyGenerator.init(keyGenParameterSpec)
        }
        keyGenerator.generateKey()
    }

    private fun getSecretKey(): SecretKey {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")

        // Before the keystore can be accessed, it must be loaded.
        keyStore.load(null)
        return keyStore.getKey("MY_TEST_KEY", null) as SecretKey
    }

    private fun getCipher(): Cipher {
        return Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                + KeyProperties.BLOCK_MODE_CBC + "/"
                + KeyProperties.ENCRYPTION_PADDING_PKCS7)
    }
    //endregion
}