package com.example.smalltests.encrypt_decrypt

import android.util.Base64
import java.io.File
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.Key
import java.security.NoSuchAlgorithmException
import java.security.spec.AlgorithmParameterSpec
import javax.crypto.Cipher
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class AESEncryptDecrypt private constructor() {

    private val secretKey: SecretKey
    private val iV: AlgorithmParameterSpec

    init {
        this.secretKey = createAESKey(randomString()) as SecretKey
        this.iV = IvParameterSpec(AES_IV.toByteArray())
    }

    /**
     * Encrypt String
     * @param stringToEncrypt : String to Encrypt
     */
    fun encryptString(stringToEncrypt: String): String {
        var cipher: Cipher? = null
        try {
            cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher?.init(Cipher.ENCRYPT_MODE, secretKey, iV)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        }
        val encryptedBytes = cipher?.doFinal(stringToEncrypt.toByteArray())
        return Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
    }

    /**
     * Decrypt String
     * @param stringToDecrypt : String to Decrypt
     */
    fun decryptString(stringToDecrypt: String): String {
        var cipher: Cipher? = null
        try {
            cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher?.init(Cipher.DECRYPT_MODE, secretKey, iV)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        }

        val decryptedBytes = cipher?.doFinal(Base64.decode(stringToDecrypt, Base64.DEFAULT))
        return String(decryptedBytes ?: ByteArray(AES_RANDOM_STRING_LENGTH))
    }


    /**
     * Encrypt File
     * @param file : File to Encrypt
     */
    fun encryptFile(file: File): ByteArray {
        var cipher: Cipher? = null
        try {
            cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher?.init(Cipher.ENCRYPT_MODE, secretKey, iV)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        }
        return cipher?.doFinal(file.readBytes()) ?: ByteArray(AES_RANDOM_STRING_LENGTH)
    }

    /**
     * Decrypt File
     * @param file : File to Decrypt
     */
    fun decryptFile(file: File): ByteArray {
        var cipher: Cipher? = null
        try {
            cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher!!.init(Cipher.DECRYPT_MODE, secretKey, iV)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        }
        return cipher!!.doFinal(file.readBytes())
    }


    companion object {

        const val AES_IV = "abcaqwerabcaqwer"
        const val AES_TRANSFORMATION = "AES/CBC/PKCS7Padding"
        const val AES_RANDOM_STRING =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$"
        const val AES_RANDOM_STRING_LENGTH = 16
        private var INSTANCE: AESEncryptDecrypt? = null

        fun getInstance() =
            INSTANCE ?: synchronized(AESEncryptDecrypt::class.java) {
                INSTANCE ?: AESEncryptDecrypt()
                    .also { INSTANCE = it }
            }

        private fun createAESKey(keyValue: String): Key {
            if (keyValue.length != AES_RANDOM_STRING_LENGTH)
                try {
                    throw Exception("Key must be exactly 16 characters")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            return SecretKeySpec(keyValue.toByteArray(), AES_TRANSFORMATION)
        }

        private fun randomString(): String {
            return "ABCDE12345ABCDE1"
            /*val AB = AES_RANDOM_STRING
            val sb = StringBuilder(AES_RANDOM_STRING_LENGTH)
            for (i in 0 until AES_RANDOM_STRING_LENGTH)
                sb.append(AB[SecureRandom().nextInt(AB.length)])
            return sb.toString()*/
        }
    }
}