package com.example.smalltests.encrypt_decrypt

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*

class EncryptDecryptActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val sb = StringBuilder()

        val text = "Plain Text"
        val textEnc = AESEncryptDecrypt.getInstance().encryptString(text)
        sb.append(text)
        sb.append("\nEncrypted: ")
        sb.append(textEnc)
        sb.append("\nDecrypted: ")
        sb.append(AESEncryptDecrypt.getInstance().decryptString(textEnc))
        sb.append("\n")


        statusTV.text = sb.toString()
    }
}