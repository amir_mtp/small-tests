package com.example.smalltests.enum_vs_sealed_test


data class User(val name: String, val age: Int)

enum class EnumTest {
    LOAD, ERROR
}
sealed class SealedTest {
    object LOAD : SealedTest()
    class SHOW_USER(val user: User) : SealedTest()
    class ERROR(val msg: String) : SealedTest()
}

fun sealedCheck(test: SealedTest) : String {
    return when (test) {
        SealedTest.LOAD -> {
            "Sealed Load : $test"
        }
        is SealedTest.ERROR -> {
            "Sealed Error : ${test.msg}"
        }
        is SealedTest.SHOW_USER -> {
            "Sealed SHOW User: ${test.user.name} ${test.user.age}"
        }
    }
}
fun enumCheck(test: EnumTest, showOrdinal: Boolean) : String {
    return when (test) {
        EnumTest.LOAD -> {
            "Enum Load : ${if(showOrdinal) "Ordinal (Sequence in Enum Declaration)" + test.ordinal else test}"
        }
        EnumTest.ERROR -> {
            "Enum Error : ${if(showOrdinal) "Ordinal (Sequence in Enum Declaration)" + test.ordinal else test}"
        }
    }
}
