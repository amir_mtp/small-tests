package com.example.smalltests.enum_vs_sealed_test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*

class EnumVsSealedTestActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

         val b = StringBuilder().apply {
            append(enumCheck(EnumTest.LOAD, false))
            append("\n\n")
            append(enumCheck(EnumTest.ERROR, true))
            append("\n\n")
            append(sealedCheck(SealedTest.ERROR("Message")))
            append("\n\n")
            append(sealedCheck(SealedTest.SHOW_USER(User("Name",45))))
        }

        statusTV.text = b.toString()
    }
}
