package com.example.smalltests.ext_functions

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.StringBuilder

class ExtensionFunctionsActivity: AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        statusTV.text = "${
            StringBuilder().apply {
                append("apply ")
                append("Testing ")
                append(".")
            }
        }\n\n${
            StringBuilder().also {
                it.append("also ")
                it.append("Testing ")
                it.append(".")
            }
        }\n\n${
            StringBuilder().let {
                it.append("let ")
                it.append("Testing ")
                it.append(".")
            }
        } ${
            StringBuilder().let {
                it.append("let ")
                it.append("Testing ")
                it.append(".")
                it.length
            }
        }\n\n${
            StringBuilder().run {
                append("Substring run ")
                append("Testing ")
                append(".")
                toString()
                substring(0,20)
            }
        } ${
            StringBuilder().run {
                append("Substring run ")
                append("Testing ")
                append(".")
                substring(0,20)
                length
            }
        }\n\n${
            with(StringBuilder()){
                append("with ")
                append("Testing ")
                append(".")
            }
        }"
    }
}