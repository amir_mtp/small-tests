package com.example.smalltests.java_value_ref_type

class ClassA(var name: String = ClassA::class.java.simpleName)

class ClassB(var name: String = ClassB::class.java.simpleName)

fun modifyClassA(classA: ClassA) : String{
    classA.name = "classA name modified"
    return classA.name
}


open class Te {
    protected open var i = 0

    fun getTeI() = i
}

open class Tee : Te() {
    fun getTeeI() = i

    override var i = 5
}

class Teee : Tee(){
    fun getTeeeI() = i

    override var i = 6
}