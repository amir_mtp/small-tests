package com.example.smalltests.java_value_ref_type

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.StringBuilder

class JavaTypeMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val sb = StringBuilder()

        val classA = ClassA()
        sb.append(classA.name)
        sb.append("\n")
        sb.append(modifyClassA(classA))
        sb.append("\n")
        sb.append(classA.name)
        sb.append("\n")

        val classB = ClassB()
        sb.append(classB.name)
        sb.append("\n")

        val te = Te()
        val tee = Tee()
        val teee = Teee()

        sb.append("\n\nJust checking nested level members modified values\n")
        sb.append("Class: ${te.getTeI()}\nNested Class: ${tee.getTeeI()}\n1 More Level: ${teee.getTeeeI()}\n\n")
        sb.append("Calling top parent member value by creating nested class objects\n")
        sb.append("Class: ${te.getTeI()}\nNested Class: ${tee.getTeI()}\n1 More Level: ${teee.getTeI()}")

        statusTV.text = sb.toString()
    }
}