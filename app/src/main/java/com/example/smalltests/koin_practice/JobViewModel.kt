package com.example.smalltests.koin_practice

import androidx.lifecycle.ViewModel

class JobViewModel(val repository: JobRepository) : ViewModel() {

    fun callApi() : String {
        return repository.callApi()
    }

    fun doSomeJob() : String {
        return "Done Some Job"
    }
}