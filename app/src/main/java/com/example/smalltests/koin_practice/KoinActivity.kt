package com.example.smalltests.koin_practice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class KoinActivity : AppCompatActivity() {

    val prefMgr by inject<PrefMgr>()
    val vm : JobViewModel by viewModel() //by viewModel always return singleton, get() or inject() will return acc to module setting
    val user : UserData = get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)
        prefMgr.putString("test", "from PrefMgr by koin")
        val user2 : UserData = get()
        val vm2 : JobViewModel by inject()

        val builder = StringBuilder().apply {
            append(prefMgr.getString("test"))
            append("\n\n")

            append(vm.doSomeJob())
            append("\n\n")

            append(vm.callApi())
            append("\n\n")

            append(user.name)
            append("\n\n")
            append(user)
            append("\n\n")
            append(user2)
            append("\n\n")
        }

        statusTV.text = builder.toString()

    }
}