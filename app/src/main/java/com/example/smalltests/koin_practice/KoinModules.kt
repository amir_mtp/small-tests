package com.example.smalltests.koin_practice

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dbModule = module {
    single { PrefMgr(get()) }   //single creates singleton
    factory { UserData("Test Name") } //factory creates new instance every time
}

val viewModelModule = module {
    factory { JobRepository() }
    viewModel { JobViewModel(get()) } //viewmodel creates new instance every time
}