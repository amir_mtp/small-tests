package com.example.smalltests.koin_practice

import android.content.Context

class PrefMgr(context : Context) {

    private val pref= context.getSharedPreferences("KoinPref", Context.MODE_PRIVATE)
    private val editor = pref.edit()


    fun putString(key: String, value: String) {
        editor.putString(key, value).apply()
    }

    fun getString(key: String) : String {
        return pref.getString(key, "")!!
    }
}