package com.example.smalltests.kotlin_tests

interface Base {
    fun invoke() : String
}

class BaseImpl(val msg: String) : Base {
    override fun invoke() : String {
        return msg
    }
}

class DerivedA : Base {
    override fun invoke() : String {
        return "printing from DerivedA"
    }
}

class DerivedB(base: Base) : Base by base

open class LazyClassWithVariable {
    open val temp : String by lazy { "Original Temp" }
}

class DerivedLazyClassWithOverridedVeriable : LazyClassWithVariable() {
    override val temp: String = "Overrided Temps"
}