package com.example.smalltests.kotlin_tests

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import kotlin.reflect.KClass

inline fun <reified viewModelType : ViewModel> AppCompatActivity.bindViewModel() =
    bindViewModel(viewModelType::class/*, viewModelFactoryProvider*/)

@PublishedApi
internal fun <viewModelT : ViewModel> AppCompatActivity.bindViewModel(viewModelType: KClass<viewModelT>)
        : Lazy<viewModelT> = lazy {
    ViewModelProviders.of(this).get(viewModelType.java)
}

class MyLazyUser(val name : String = "Amir")