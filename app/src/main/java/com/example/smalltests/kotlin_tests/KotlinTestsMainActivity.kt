package com.example.smalltests.kotlin_tests

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import com.example.smalltests.util.Applog
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.StringBuilder

class KotlinTestsMainActivity : AppCompatActivity() {

    inline val d: Int
        get() = 4

    private val lazyInt by lazy { 4 }
    private val lazyViewModel by bindViewModel<LazyViewModel>()

    /*  The default value SYNCHRONIZED will ensure only a single thread can initialize the property
        using locks. If we are sure the property will only be accessed by a single thread we can
        switch to NONE to avoid the overhead of performing the synchronization. There is also the
        option of using PUBLICATION which allows multiple threads to call the initializer, but only
        the first returned value being used
    */
    private val lazyUser by lazy(LazyThreadSafetyMode.NONE) { MyLazyUser() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val sb = StringBuilder()
        sb.append("Starting...\n")

        a(fun() {
            sb.append("fun test 1 Testing")
            sb.append("\n")
        })

        a {
            sb.append("fun test 2 Testing")
            sb.append("\n")
        }

        onload {
            sb.append("fun test 3 Testing")
            sb.append("\n")
            sb.append(it)
            sb.append("\n")
        }

        sb.append("Returned int from fun: ${onClock {
            4
        }}")

        sb.append("\n")
        sb.append("Returned int from inlined fun: ${onClockInlined {
            42
        }}")

        sb.append("\n")
        sb.append(isWorkDone {
            sb.append("\n")
            sb.append("From inside isWorkDone calling")
            sb.append("\n")
            sb.append("Appending returned Char: `$it`")
            false
        })

        sb.append("\n")
        sb.append(someMethod(4) {
            sb.append("From someMethod ")
        })

        sb.append("\n")
        sb.append(foo({
            sb.append("hello")
            true
        }) {
            sb.append(" world")
            sb.append("\n")
            "Return from foo b()"
        })


        sb.append("\n\n")
        sb.append("lazy initialized : $lazyInt")
        sb.append("\n")
        sb.append("lazy viewmodel initialized : ${lazyViewModel.test.value}")
        sb.append("\n")
        sb.append("lazy user initialized : ${lazyUser.name}")

        sb.append("\n\n")
        sb.append("Deligation : DerivedA > ${DerivedA().invoke()}")
        sb.append("\n")
        sb.append("Deligation : DerivedB > ${DerivedB(BaseImpl("from deligated by")).invoke()}")

        sb.append("\n\n")
        sb.append("LazyClassWithVariable > ${LazyClassWithVariable().temp}")
        sb.append("\n")
        sb.append("LazyClassWithVariable > ${DerivedLazyClassWithOverridedVeriable().temp}")


        statusTV.text = sb.toString()
    }

    private fun a(c: () -> Unit) {
        c()
    }

    private fun onload(loaded: (m: String) -> Unit) {
        loaded("From onload")
    }

    private fun onClock(clicked: () -> Int) {
        clicked()
    }

    private inline fun onClockInlined(inlinedClicked: () -> Int) {
        inlinedClicked()
    }

    private fun isWorkDone(done: (isDone: Char) -> Boolean) {
        Applog.toast("From isWorkDone fun.. Do your task here")
        done('O')
    }

    private fun someMethod(a: Int, func: () -> Unit): Int {
        func()
        return 2 * a
    }

    private fun foo(a: () -> Boolean, b: () -> String) {
        a()
        b()
    }
}