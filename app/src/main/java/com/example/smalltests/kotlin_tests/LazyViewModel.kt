package com.example.smalltests.kotlin_tests

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LazyViewModel : ViewModel() {

    var test = MutableLiveData<String>().apply { value = "from LazyViewModel" }
}