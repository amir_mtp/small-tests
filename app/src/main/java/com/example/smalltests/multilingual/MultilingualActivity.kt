package com.example.smalltests.multilingual

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_multilingual.*
import android.widget.Toast
import com.example.smalltests.MainActivity
import android.content.Intent
import android.util.DisplayMetrics
import com.example.smalltests.R
import java.util.*


class MultilingualActivity : AppCompatActivity() {
    /*
    * https://github.com/anurajr1/Multi-language_App/blob/master/app/src/main/java/com/anu/developers3k/languagechange/MainActivity.java
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multilingual)
        engBtn.setOnClickListener {
            setLocale("en")
        }
        urduBtn.setOnClickListener {
            setLocale("ur")
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocalHelper.onAttach(newBase))
    }

    fun setLocale(localeName: String) {
        LocalHelper.setLocale(this, localeName)
        val refresh = Intent(this, MainActivity::class.java)
        refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(refresh)
    }
}