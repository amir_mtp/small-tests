package com.example.smalltests.mvvm

interface Callback<T> {
    fun success(obj: T)
    fun fail(msg: String)
}