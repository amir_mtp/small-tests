package com.example.smalltests.mvvm

import android.os.Handler
import androidx.lifecycle.MutableLiveData

class LocalDataSource : Source {

    val loginResponse = MutableLiveData<LoginResponse>()
    val status = MutableLiveData<String>()

    override fun login(id: String, pwd: String, callback: Callback<LoginResponse>?) {
        Handler().postDelayed({
            if (MyViewModel.testId == id && MyViewModel.testPwd == pwd) {
                loginResponse.value = LoginResponse("Success", "From Local")
                status.value = "${loginResponse.value?.status}: ${loginResponse.value?.msg}"
            } else {
                loginResponse.value = LoginResponse("Failed", "From Local: invalid Id/Pwd")
                status.value = "${loginResponse.value?.status}: ${loginResponse.value?.msg}"
            }
        }, 1000)
    }
}