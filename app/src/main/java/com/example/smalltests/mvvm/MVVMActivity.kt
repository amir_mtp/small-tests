package com.example.smalltests.mvvm

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.smalltests.R
import com.example.smalltests.databinding.ActivityMvvmBinding
import com.example.smalltests.util.Applog

class MVVMActivity : AppCompatActivity() {

    lateinit var binding : ActivityMvvmBinding
    lateinit var vm : MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mvvm)
        vm = ViewModelProviders.of(this).get(MyViewModel::class.java)
        binding.viewmodel = vm
        binding.lifecycleOwner = this

        vm.loginResponse.observe(this, Observer {
            vm.isApiCalling.value = false
            if(it == null)
                Applog.toast("Refreshed")
        })

    }
}