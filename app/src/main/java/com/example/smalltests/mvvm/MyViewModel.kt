package com.example.smalltests.mvvm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MyViewModel : ViewModel() {
    var name = MutableLiveData<String>().apply { value = "Name" }
    var song = MutableLiveData<Song>().apply { value = Song() }

    fun changeSongName(){
        name.value = "Name Changed"
        song.value?.name = "Song Name Changed"
        song.value?.notifyChange()
    }

    companion object{
        val testId = "test"
        val testPwd = "1234"
    }

    private val repo = Repository()

    val id = MutableLiveData<String>().apply { value = "" }
    val pwd = MutableLiveData<String>().apply { value = "" }
    val isApiCalling = MutableLiveData<Boolean>()

    val status = repo.status
    val loginResponse = repo.loginResponse

    fun login(){
        isApiCalling.value = true
        repo.login(id.value!!, pwd.value!!, null)
    }

    fun refresh() = repo.refresh()
}