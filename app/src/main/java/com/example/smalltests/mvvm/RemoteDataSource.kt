package com.example.smalltests.mvvm

import android.os.Handler

class RemoteDataSource : Source {

    override fun login(id: String, pwd: String, callback: Callback<LoginResponse>?) {
        Handler().postDelayed({
            if(MyViewModel.testId == id && MyViewModel.testPwd == pwd){
                callback?.success(LoginResponse("Success", "From Remote"))
            } else {
                callback?.fail("From Remote: Invalid id/pwd")
            }
        }, 1000)
    }
}