package com.example.smalltests.mvvm


class Repository : Source {

    private val sourceL = LocalDataSource()
    private val sourceR = RemoteDataSource()

    val loginResponse = sourceL.loginResponse
    val status = sourceL.status

    override fun login(id: String, pwd: String, callback: Callback<LoginResponse>?) {
        if (loginResponse.value == null) {
            sourceR.login(id, pwd, object : Callback<LoginResponse>{
                override fun success(obj: LoginResponse) {
                    loginResponse.value = obj
                    status.value = "${loginResponse.value?.status}: ${loginResponse.value?.msg}"
                }

                override fun fail(msg: String) {
                    loginResponse.value = LoginResponse("Failed", msg)
                    status.value = "${loginResponse.value?.status}: ${loginResponse.value?.msg}"
                }
            })
        } else {
            sourceL.login(id, pwd, null)
        }
    }

    fun refresh(){
        loginResponse.value = null
    }
}