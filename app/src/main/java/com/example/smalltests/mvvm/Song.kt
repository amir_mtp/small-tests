package com.example.smalltests.mvvm

import androidx.databinding.BaseObservable

data class Song(
    var name: String = "Test Name",
    var artist: String? = null,
    var genre: String? = null,
    var length: Int = 10) : BaseObservable()