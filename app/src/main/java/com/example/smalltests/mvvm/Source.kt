package com.example.smalltests.mvvm

interface Source {
    fun login(id: String, pwd: String, callback: Callback<LoginResponse>?)
}