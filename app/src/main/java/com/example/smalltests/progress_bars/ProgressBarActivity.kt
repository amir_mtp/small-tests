package com.example.smalltests.progress_bars

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.smalltests.R
import com.example.smalltests.util.Applog
import kotlinx.android.synthetic.main.activity_progress_bars.*
import java.util.*

class ProgressBarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress_bars)

        var p4 = 0
        Timer().schedule(object : TimerTask(){
            override fun run() {
                progressBar4.progress = p4
                p4 += 10
                if(p4 >= 100)
                    p4 = 0
            }
        }, 2000, 500)

        ObjectAnimator.ofInt(progressBar6, "progress", progressBar6.progress, 100).apply {
            duration = 5000
            interpolator = LinearInterpolator()
            repeatMode = ValueAnimator.RESTART
            repeatCount = ValueAnimator.INFINITE
            addUpdateListener {
                Applog.d("${it.animatedFraction * 100}")
                when {
                    (it.animatedFraction*100) > 50 -> {
                        progressBar6.progressDrawable = ContextCompat.getDrawable(this@ProgressBarActivity, R.drawable.pb_two_color)
                    }
                    else -> {
                        progressBar6.progressDrawable = ContextCompat.getDrawable(this@ProgressBarActivity, R.drawable.pb_two_color_b)
                    }
                }

            }
            start()
        }
    }
}