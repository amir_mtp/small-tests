package com.example.smalltests.ssl_pinning_test

import com.example.smalltests.AppClass

import com.example.smalltests.R

import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

import okhttp3.OkHttpClient

object CertificatePinningClass {
    fun addSslCertificates(okHttpClient: OkHttpClient.Builder) {
        var sslContext: SSLContext? = null
        try {
            // loading CAs from an InputStream
            val cf = CertificateFactory.getInstance("X.509")
            val cert = AppClass.ctx.resources.openRawResource(R.raw.localhost_5001)
            val ca: Certificate
            try {
                ca = cf.generateCertificate(cert)
            } finally {
                cert.close()
            }

            // creating a KeyStore containing our trusted CAs
            val keyStoreType = KeyStore.getDefaultType()
            val keyStore = KeyStore.getInstance(keyStoreType)
            keyStore.load(null, null)
            keyStore.setCertificateEntry("ca", ca)

            // creating a TrustManager that trusts the CAs in our KeyStore
            val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
            val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
            tmf.init(keyStore)

            // creating an SSLSocketFactory that uses our TrustManager
            sslContext = SSLContext.getInstance("TLS")
            sslContext!!.init(null, tmf.trustManagers, null)

            okHttpClient.sslSocketFactory(
                sslContext.socketFactory,
                tmf.trustManagers[0] as X509TrustManager
            )

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
