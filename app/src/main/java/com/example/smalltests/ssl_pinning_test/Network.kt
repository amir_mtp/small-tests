package com.example.smalltests.ssl_pinning_test

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

object Network {
//    const val SECURE_LOCALHOST = "https://localhost:5001"
    const val SECURE_LOCALHOST = "https://ed85be64.ngrok.io"
//    const val UNSECURE_LOCALHOST = "http://localhost:5000"
    const val UNSECURE_LOCALHOST = "http://96019513.ngrok.io"

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }

    fun  getClientWithCertFilePinning(url : String) : Services{
        val builder = OkHttpClient.Builder()

        //Add certificate for pinning
        CertificatePinningClass.addSslCertificates(builder)

        return Retrofit.Builder()
            .client(builder.apply {
                readTimeout(1, TimeUnit.MINUTES)
                writeTimeout(1, TimeUnit.MINUTES)
                connectTimeout(1, TimeUnit.MINUTES)
                addInterceptor(loggingInterceptor)
            }.build())
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Services::class.java)
    }

    fun getClientWithPublicKeyPinning() {

    }

    fun getClientWithSPKIPinning() {

    }

    fun getClientWithNetworkSecurityConfigPinning() {

    }

    interface Services {
        @GET("/")
        fun hitLocalhost() : Call<ResponseBody>
    }
}