package com.example.smalltests.ssl_pinning_test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_ssl_pinning_test.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

//TODO Run localhost in mobile via adb reverse tcp:5000 tcp:5000 [enter your desired port to runs]
//TODO this will run on browser not in app, to run on app, use ngrok for port forwarding

class SSLPinningTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ssl_pinning_test)

        certificatePinningBtn.setOnClickListener {
            Network.getClientWithCertFilePinning(Network.SECURE_LOCALHOST)
                .hitLocalhost().enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        networkStatusTV.text = "onResponse: ${response.body()?.string()}"
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        networkStatusTV.text = "onError: ${t.message}"
                    }
                })
        }
    }
}