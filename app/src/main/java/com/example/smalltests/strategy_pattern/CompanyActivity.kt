package com.example.smalltests.strategy_pattern

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import java.lang.StringBuilder

class CompanyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        val sb = StringBuilder()

        val asus = AsusCompany()
        val dell = DellCompany()

        sb.append("Name: ${asus.getLaptop().brandName()}\nModel: ${asus.getLaptop().model()}\nPrice: ${asus.getLaptop().price()}")
        sb.append("\n\n")
        sb.append("Mouse Type: ${dell.getMouse().type()}\nMouse Size: ${dell.getMouse().size()}\nMouse Color: ${dell.getMouse().color()}")
        sb.append("\n\n")
        sb.append("Name: ${dell.getLaptop().brandName()}\nModel: ${dell.getLaptop().model()}\nPrice: ${dell.getLaptop().price()}")
        sb.append("\n\n")

        val customAsus = Asus().apply {
            name = "Customize Asus"
            price = 2909349
            model = "Asus X 03902"
        }

        asus.setLaptop(customAsus)

        val customDellMouse = DellMouse("Silver", MouseSize.LARGE, "Wireless")

        dell.setMouse(customDellMouse)
        sb.append("Mouse Type: ${dell.getMouse().type()}\nMouse Size: ${dell.getMouse().size()}\nMouse Color: ${dell.getMouse().color()}")
        sb.append("\n\n")

        sb.append("Name: ${asus.getLaptop().brandName()}\nModel: ${asus.getLaptop().model()}\nPrice: ${asus.getLaptop().price()}")
        sb.append("\n\n")

        val mac = MacCompany()

        sb.append("Name: ${mac.getLaptop().brandName()}\nModel: ${mac.getLaptop().model()}\nPrice: ${mac.getLaptop().price()}")
        sb.append("\n\n")

        val customMac = mac.getLaptop().custom("Custom Mac", 94039, "2019")

        sb.append("Name: ${customMac.brandName()}\nModel: ${customMac.model()}\nPrice: ${customMac.price()}")
        sb.append("\n\n")

        sb.append("Name: ${mac.getLaptop().brandName()}\nModel: ${mac.getLaptop().model()}\nPrice: ${mac.getLaptop().price()}")
        sb.append("\n\n")

        statusTV.text = sb.toString()
    }
}