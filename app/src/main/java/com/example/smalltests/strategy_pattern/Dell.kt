package com.example.smalltests.strategy_pattern

class Dell(var name: String = Dell::class.java.simpleName,
           var price: Int = 23450,
           var model: String = "N/A") : Laptop {

    override fun brandName() = name

    override fun price() = price

    override fun model() = model

    override fun custom(name: String, price: Int, model: String): Laptop {
        return Dell(name, price, model)
    }

}