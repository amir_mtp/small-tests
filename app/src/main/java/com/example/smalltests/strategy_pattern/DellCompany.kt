package com.example.smalltests.strategy_pattern

class DellCompany : ManufacturingCompany() {
    init {
        mLaptop = Dell()
        mMouse = DellMouse()
    }
}