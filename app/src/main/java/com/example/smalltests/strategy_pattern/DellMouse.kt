package com.example.smalltests.strategy_pattern

class DellMouse(val color: String = "Black", val size: MouseSize = MouseSize.MEDIUM, val type : String = "Gaming") : Mouse {

    override fun color() = color

    override fun size() : String {
        return when (size) {
            MouseSize.MINI -> "Mini"
            MouseSize.MEDIUM -> "Medium"
            MouseSize.LARGE -> "Large"
        }
    }

    override fun type() = this.type

}