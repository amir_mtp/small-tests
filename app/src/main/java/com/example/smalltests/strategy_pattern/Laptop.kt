package com.example.smalltests.strategy_pattern

interface Laptop {
    fun brandName() : String
    fun price() : Int
    fun model() : String
    fun custom(name: String, price: Int, model: String) : Laptop
}