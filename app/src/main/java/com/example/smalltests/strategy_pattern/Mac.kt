package com.example.smalltests.strategy_pattern

class Mac(var name: String = Mac::class.java.simpleName,
          var price: Int = 9403,
          var model: String = "XS MacBook Pro") : Laptop {

    override fun brandName() = name

    override fun price() = price

    override fun model() = model

    override fun custom(name: String, price: Int, model: String) = Mac(name, price, model)
}