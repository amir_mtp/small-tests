package com.example.smalltests.strategy_pattern

class MacCompany : ManufacturingCompany() {
    init {
        mLaptop = Mac()
    }
}