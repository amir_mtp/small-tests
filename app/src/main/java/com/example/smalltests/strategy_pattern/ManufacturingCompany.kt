package com.example.smalltests.strategy_pattern

open class ManufacturingCompany {

    protected lateinit var mLaptop : Laptop
    protected lateinit var mMouse : Mouse

    fun getLaptop() : Laptop {
        return mLaptop
    }

    fun setLaptop(laptop: Laptop){
        mLaptop = laptop
    }

    fun getMouse() = mMouse

    fun setMouse(mouse: Mouse) {
        mMouse = mouse
    }
}