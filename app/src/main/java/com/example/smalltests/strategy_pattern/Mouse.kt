package com.example.smalltests.strategy_pattern

interface Mouse {
    fun color() : String
    fun size() : String
    fun type() : String
}