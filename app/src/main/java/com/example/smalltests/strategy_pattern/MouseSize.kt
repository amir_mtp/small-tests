package com.example.smalltests.strategy_pattern

enum class MouseSize(val type: Int) {
    MINI(0),
    MEDIUM(1),
    LARGE(2);
}