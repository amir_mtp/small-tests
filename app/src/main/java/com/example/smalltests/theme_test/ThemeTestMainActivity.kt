package com.example.smalltests.theme_test

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*

class ThemeTestMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_tv)

        statusTV.text = "Open Theme Setting Activity"
        statusTV.textSize = 25f
        statusTV.setBackgroundColor(ContextCompat.getColor(this, R.color.text_background))
        statusTV.setTextColor(ContextCompat.getColor(this, R.color.text_color))
        statusTV.setOnClickListener {
            startActivity(Intent(this, ThemeTestSettingActivity::class.java))
        }
    }
}