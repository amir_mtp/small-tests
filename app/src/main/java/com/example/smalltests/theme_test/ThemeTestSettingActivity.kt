package com.example.smalltests.theme_test

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.example.smalltests.R
import kotlinx.android.synthetic.main.activity_single_tv.*
import kotlinx.android.synthetic.main.activity_theme_test_setting.*

class ThemeTestSettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_theme_test_setting)

        themeSettingBg.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        darkRB.setTextColor(ContextCompat.getColor(this, R.color.text_color))
        lightRB.setTextColor(ContextCompat.getColor(this, R.color.text_color))

        darkRB.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        lightRB.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }
}