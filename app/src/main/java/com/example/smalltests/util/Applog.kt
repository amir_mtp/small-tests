package com.example.smalltests.util

import android.util.Log
import android.widget.Toast
import com.example.smalltests.AppClass

object Applog {
    fun toast(msg: String){
        Toast.makeText(AppClass.ctx, msg, Toast.LENGTH_SHORT).show()
    }

    fun d(msg: String){
        Log.d("TAG", msg)
    }
}