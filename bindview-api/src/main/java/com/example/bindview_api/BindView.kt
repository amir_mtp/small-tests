package com.example.bindview_api


@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FIELD)
annotation class BindView(val value: Int)
