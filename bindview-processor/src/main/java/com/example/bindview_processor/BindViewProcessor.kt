package com.example.bindview_processor

import com.example.bindview_api.BindView
import com.example.bindview_processor.model.BindingSet
import com.example.bindview_processor.model.ClassBinding
import com.example.bindview_processor.model.PackageBinding
import com.example.bindview_processor.model.ViewBinding
import com.squareup.javapoet.ClassName
import com.squareup.javapoet.JavaFile
import com.squareup.javapoet.MethodSpec
import com.squareup.javapoet.TypeSpec

import java.io.IOException
import java.util.Collections
import java.util.HashSet
import java.util.function.Consumer

import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Filer
import javax.annotation.processing.Messager
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.Modifier
import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic

import javax.lang.model.element.Modifier.FINAL
import javax.lang.model.element.Modifier.PRIVATE
import javax.lang.model.element.Modifier.PUBLIC
import javax.lang.model.element.Modifier.STATIC

class BindViewProcessor : AbstractProcessor() {

    override fun getSupportedAnnotationTypes(): Set<String> {
        return Collections.singleton(BindView::class.java.canonicalName)
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(set: Set<TypeElement>, roundEnvironment: RoundEnvironment): Boolean {
        val bindingSet = buildBindingSet(roundEnvironment)
        val javaFiles = generateBinderClasses(bindingSet)
        writeFiles(javaFiles)
        return true
    }

    private fun buildBindingSet(roundEnvironment: RoundEnvironment): BindingSet {
        val bindingSet = BindingSet()

        val elementsToBind = roundEnvironment.getElementsAnnotatedWith(BindView::class.java)
        elementsToBind.forEach { element -> addElementBinding(bindingSet, element) }

        return bindingSet
    }


    private fun addElementBinding(bindingSet: BindingSet, element: Element) {
        val elementBinding = getViewBinding(element) ?: return

        val classElement = element.enclosingElement as TypeElement
        val packageElement = getPackage(classElement)

        bindingSet.addBinding(packageElement, classElement, elementBinding)
    }

    private fun getPackage(element: Element): PackageElement {
        var element = element
        while (element.kind != ElementKind.PACKAGE) {
            element = element.enclosingElement
        }

        return element as PackageElement
    }

    private fun getViewBinding(element: Element): ViewBinding? {
        if (!isFieldAccessible(element)) {
            val messager = processingEnv.messager
            messager.printMessage(
                Diagnostic.Kind.ERROR,
                "Field not accessible, it cannot be private or static to bind"
            )
            return null
        }

        val annotation = element.getAnnotation(BindView::class.java)
        val viewId = annotation.value
        val type = element.asType()
        val name = element.simpleName.toString()

        return ViewBinding(type, name, viewId)
    }

    private fun isFieldAccessible(element: Element): Boolean {
        val modifiers = element.modifiers
        return !modifiers.contains(PRIVATE) && !modifiers.contains(STATIC)
    }

    private fun generateBinderClasses(bindingSet: BindingSet): Set<JavaFile> {
        val files = HashSet<JavaFile>()

        for (packageBinding in bindingSet.getPackageBindings()) {
            val packageName = packageBinding.getPackageName()
            val binderClass = generateBinderClass(packageBinding)

            val javaFile = JavaFile.builder(packageName, binderClass).build()
            files.add(javaFile)
        }

        return files
    }

    private fun generateBinderClass(packageBinding: PackageBinding): TypeSpec {
        val constructor = MethodSpec.constructorBuilder()
            .addModifiers(PRIVATE)
            .build()

        val classBuilder = TypeSpec.classBuilder("ViewBinder")
            .addModifiers(FINAL)
            .addMethod(constructor)

        packageBinding.getClassBindings()
            .stream()
            .map { this.generateBindMethod(it) }
            .forEach { classBuilder.addMethod(it) }

        return classBuilder.build()
    }

    private fun generateBindMethod(classBinding: ClassBinding): MethodSpec {
        val methodBuilder = MethodSpec.methodBuilder("bind")
            .addModifiers(PUBLIC, STATIC)
            .addParameter(ClassName.get(classBinding.classElement), "target")

        for (elementBinding in classBinding.viewBindings) {
            methodBuilder.addStatement(
                "target.\$N = (\$T) target.findViewById(\$L)",
                elementBinding.name,
                ClassName.get(elementBinding.type),
                elementBinding.viewId
            )
        }

        return methodBuilder.build()
    }

    private fun writeFiles(javaFiles: Collection<JavaFile>) {
        javaFiles.forEach(Consumer<JavaFile> { this.writeFile(it) })
    }

    private fun writeFile(javaFile: JavaFile) {
        val filer = processingEnv.filer

        try {
            javaFile.writeTo(filer)
        } catch (e: IOException) {
            val messager = processingEnv.messager
            val message = String.format("Unable to write file: %s", e.message)
            messager.printMessage(Diagnostic.Kind.ERROR, message)
        }

    }
}
