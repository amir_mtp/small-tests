package com.example.bindview_processor.model

import java.util.HashMap

import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement

class BindingSet {
    private val packageBindingMap: MutableMap<PackageElement, PackageBinding>

    init {
        this.packageBindingMap = HashMap()
    }

    fun getPackageBindings(): Collection<PackageBinding> {
        return packageBindingMap.values
    }

    fun getPackageBinding(packageElement: PackageElement): PackageBinding? {
        return (packageBindingMap as java.util.Map<PackageElement, PackageBinding>).computeIfAbsent(
            packageElement
        ) { PackageBinding(it) }
    }

    fun addBinding(
        packageElement: PackageElement, classElement: TypeElement,
        elementBinding: ViewBinding
    ) {
        val packageBinding = getPackageBinding(packageElement)
        val classBinding = packageBinding?.getClassBinding(classElement)
        classBinding?.addViewBinding(elementBinding)
    }
}
