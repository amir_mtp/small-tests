package com.example.bindview_processor.model

import java.util.HashSet

import javax.lang.model.element.TypeElement

class ClassBinding(val classElement: TypeElement) {
    private val viewBindingSet: MutableSet<ViewBinding>

    val viewBindings: Set<ViewBinding>
        get() = viewBindingSet

    init {
        this.viewBindingSet = HashSet()
    }

    fun addViewBinding(viewBinding: ViewBinding) {
        viewBindingSet.add(viewBinding)
    }
}
