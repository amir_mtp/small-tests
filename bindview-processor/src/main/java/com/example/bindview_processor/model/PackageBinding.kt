package com.example.bindview_processor.model

import java.util.HashMap

import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement

class PackageBinding(private val packageElement: PackageElement) {
    private val classBindings: MutableMap<TypeElement, ClassBinding>

    init {
        this.classBindings = HashMap()
    }

    fun getPackageName(): String {
        return packageElement.qualifiedName.toString()
    }

    fun getClassBindings(): Collection<ClassBinding> {
        return classBindings.values
    }

    fun getClassBinding(typeElement: TypeElement): ClassBinding {
        return (classBindings as java.util.Map<TypeElement, ClassBinding>).computeIfAbsent(
            typeElement) { ClassBinding(it) }
    }
}
