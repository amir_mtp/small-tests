package com.example.bindview_processor.model

import javax.lang.model.type.TypeMirror

class ViewBinding(val type: TypeMirror, val name: String, val viewId: Int)
